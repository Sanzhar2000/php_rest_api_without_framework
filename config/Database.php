<?php

class Database {
    private $host = "localhost";
    private $user = "";
    private $db = "postgres";
    private $pwd = "";
    private $conn = NULL;

    public function connect() {

        try{
            $this->conn = new PDO("pgsql:host=$this->host;port=5432;dbname=$this->db;");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch(PDOException $e) {
            echo "Connection Error: " . $e->getMessage();
        }

        return $this->conn;
    }
}
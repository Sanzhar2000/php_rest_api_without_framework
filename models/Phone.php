<?php

include_once 'Base.php';


class Phone extends Base {

    public $phone;

    public function fetchAll($phone_id) {
        $stmt = $this->conn->prepare('SELECT * FROM phones');
        $stmt->execute();
        return $stmt;
    }

    public function fetchOne($id) {

        $stmt = $this->conn->prepare('SELECT  * FROM phones WHERE id = ?');
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if($stmt->rowCount() > 0) {

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row['id'];
            $this->phone = $row['phone'];

            return TRUE;

        }

        return FALSE;
    }

    public function fetchAllSearchBySubstringPhone($substr_phone) {
        $substr_phone .= '%';
        $stmt = $this->conn->prepare('SELECT * FROM phones where phone ilike :search');
        $stmt->bindParam(':search', $substr_phone);

        $stmt->execute();
        return $stmt;
    }

    public function postData() {

        $stmt = $this->conn->prepare('INSERT INTO phones (phone) VALUES (:phone)');

        $stmt->bindParam(':phone', $this->phone);

        if($stmt->execute()) {
            return TRUE;
        }

        return FALSE;
    }
}
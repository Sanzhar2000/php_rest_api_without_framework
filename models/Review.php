<?php

include_once 'Base.php';


class Review extends Base {

    public $phone_id;
    public $text;
    public $rating;
    public $user_id;

//    public function fetchAll($phone_id) {
//        $stmt = $this->conn->prepare('SELECT * FROM users');
//        $stmt->execute();
//        return $stmt;
//    }
//
//    public function fetchOne($id) {
//
//        $stmt = $this->conn->prepare('SELECT  * FROM users WHERE id = ?');
//        $stmt->bindParam(1, $id);
//        $stmt->execute();
//
//        if($stmt->rowCount() > 0) {
//
//            $row = $stmt->fetch(PDO::FETCH_ASSOC);
//
//            $this->id = $row['id'];
//            $this->name = $row['name'];
//            $this->age = $row['age'];
//
//            return TRUE;
//
//        }
//
//        return FALSE;
//    }

    public function fetchAllByPhoneId($phone_id) {
        $stmt = $this->conn->prepare('SELECT * FROM reviews WHERE phone_id = ?');
        $stmt->bindParam(1, $phone_id);
        $stmt->execute();
        return $stmt;

    }

    public function fetchAllCount($phone_id) {
        $stmt = $this->conn->prepare('SELECT count(*) FROM reviews WHERE phone_id = ?');
        $stmt->bindParam(1, $phone_id);
        $stmt->execute();
        return $stmt;
    }

    public function postData() {

        $stmt = $this->conn->prepare('INSERT INTO reviews (phone_id, text, rating, user_id) VALUES (:phone_id, :text, :rating, :user_id)');

        $stmt->bindParam(':phone_id', $this->phone_id);
        $stmt->bindParam(':text', $this->text);
        $stmt->bindParam(':rating', $this->rating);
        $stmt->bindParam(':user_id', $this->user_id);

        if($stmt->execute()) {
            return TRUE;
        }

        return FALSE;
    }

    public function putData() {

        $stmt = $this->conn->prepare('UPDATE reviews SET phone_id = :phone_id, text = :text, rating = :rating, user_id = :user_id WHERE id = :id');

        $stmt->bindParam(':phone_id', $this->phone_id);
        $stmt->bindParam(':text', $this->text);
        $stmt->bindParam(':rating', $this->rating);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->bindParam(':id', $this->id);

        if($stmt->execute()) {
            return TRUE;
        }

        return FALSE;
    }

}
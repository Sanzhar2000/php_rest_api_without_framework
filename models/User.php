<?php

include_once 'Base.php';


class User extends Base {

    public $name;
    public $age;

    public function fetchAll() {
        $stmt = $this->conn->prepare('SELECT * FROM users');
        $stmt->execute();
        return $stmt;
    }

    public function fetchOne($id) {

        $stmt = $this->conn->prepare('SELECT  * FROM users WHERE id = ?');
        $stmt->bindParam(1, $id);
        $stmt->execute();

        if($stmt->rowCount() > 0) {

            $row = $stmt->fetch(PDO::FETCH_ASSOC);

            $this->id = $row['id'];
            $this->name = $row['name'];
            $this->age = $row['age'];

            return TRUE;

        }

        return FALSE;
    }
}
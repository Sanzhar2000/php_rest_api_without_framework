<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');

include_once '../../config/Database.php';
include_once '../../models/Review.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $db = new Database();
    $db = $db->connect();

    $review = new Review($db);

    $data = json_decode(file_get_contents("php://input"));

    $review->phone_id = $data->phone_id;
    $review->text = $data->text;
    $review->rating = $data->rating;
    $review->user_id = $data->user_id;

    if($review->postData()) {
        echo json_encode(array('message' => 'Review added'));
    } else {
        echo json_encode(array('message' => 'Review Not added, try again!'));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');

include_once '../../config/Database.php';
include_once '../../models/Review.php';

if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    $db = new Database();
    $db = $db->connect();

    $review = new Review($db);

    $data = json_decode(file_get_contents("php://input"));

    $review->id = isset($data->id) ? $data->id : NULL;
    $review->phone_id = $data->phone_id;
    $review->text = $data->text;
    $review->user_id = $data->user_id;
    $review->rating = $data->rating;

    if(! is_null($review->id)) {

        if($review->putData()) {
            echo json_encode(array('message' => 'Review updated'));
        } else {
            echo json_encode(array('message' => 'Review Not updated, try again!'));
        }
    } else {
        echo json_encode(array('message' => "Error: Review ID is missing!"));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Review.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $db = new Database();
    $db = $db->connect();

    $review = new Review($db);

    $data = json_decode(file_get_contents("php://input"));

    if(isset($data->phone_id)) {
        $res = $review->fetchAllByPhoneId($data->phone_id);
        $resCount = $res->rowCount();

        if($resCount > 0) {

            $reviews = array();

            while($row = $res->fetch(PDO::FETCH_ASSOC)) {

                extract($row);
                if ($user_id == null) {
                    $user_id = "ANONYMOUS";
                }
                array_push($reviews, array('id' => $id, 'phone_id' => $phone_id, 'text' => $text, 'user_id' => $user_id, 'rating' => $rating));
            }

            echo json_encode($reviews);

        } else {
            echo json_encode(array('message' => "No records found!"));
        }

    } else {
        echo json_encode(array('message' => "Error: User ID is missing!"));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
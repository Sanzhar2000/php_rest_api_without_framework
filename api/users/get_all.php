<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/User.php';

require "../../predis/autoload.php";
Predis\Autoloader::register();
try {
    $redis = new  Predis\Client();
} catch (Exception $e) {
    die($e->getMessage());
}


if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    if ($redis->exists("all_users")) {
        echo $redis->get("all_users");
    } else {
        $db = new Database();
        $db = $db->connect();

        $user = new User($db);

        $res = $user->fetchAll();
        $resCount = $res->rowCount();

        if($resCount > 0) {

            $users = array();

            while($row = $res->fetch(PDO::FETCH_ASSOC)) {

                extract($row);
                array_push($users, array( 'id' => $id, 'name' => $name, 'age' => $age));
            }
            echo json_encode($users);
            $redis->set("all_users", json_encode($users));
        } else {
            echo json_encode(array('message' => "No records found!"));
        }
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
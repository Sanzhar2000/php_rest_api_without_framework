<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/User.php';

if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $db = new Database();
    $db = $db->connect();

    $user = new User($db);

    $data = json_decode(file_get_contents("php://input"));

    if(isset($data->id)) {
        if($user->fetchOne($data->id)) {

            print_r(json_encode(array(
                'id' => $user->id,
                'name' => $user->name,
                'age' => $user->age
            )));

        } else {
            echo json_encode(array('message' => "No records found!"));
        }

    } else {
        echo json_encode(array('message' => "Error: User ID is missing!"));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');
header('Access-Control-Allow-Methods: POST');

include_once '../../config/Database.php';
include_once '../../models/Phone.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {

    $db = new Database();
    $db = $db->connect();

    $phone = new Phone($db);

    $data = json_decode(file_get_contents("php://input"));

    $phone->phone = $data->phone;

    if($phone->postData()) {
        echo json_encode(array('message' => 'Phone added'));
    } else {
        echo json_encode(array('message' => 'Phone Not added, try again!'));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
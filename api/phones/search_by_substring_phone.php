<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../../config/Database.php';
include_once '../../models/Phone.php';
include_once '../../models/Review.php';


if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $db = new Database();
    $db = $db->connect();

    $phone = new Phone($db);
    $review = new Review($db);

    $data = json_decode(file_get_contents("php://input"));

    if(isset($data->sub_str_phone)) {
        $res = $phone->fetchAllSearchBySubstringPhone($data->sub_str_phone);
        $resCount = $res->rowCount();

        if($resCount > 0) {

            $phones = array();

            while($row = $res->fetch(PDO::FETCH_ASSOC)) {
                extract($row);

                $res = $review->fetchAllCount($id);
                $reviews_count = extract($res->fetch(PDO::FETCH_ASSOC));

                array_push($phones, array('id' => $id, 'phone' => $phone, 'reviews_count' => $reviews_count));
            }
            echo json_encode($phones);

        } else {
            echo json_encode(array('message' => "No records found!"));
        }

    } else {
        echo json_encode(array('message' => "Error: Phone sub_str is missing!"));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
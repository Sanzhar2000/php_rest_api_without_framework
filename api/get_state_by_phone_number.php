<?php
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json');

include_once '../config/Database.php';
include_once '../models/FPhoneCodeDictionary.php';


if ($_SERVER['REQUEST_METHOD'] === 'GET') {

    $db = new Database();
    $db = $db->connect();

    $fetchStateByCode = new FPhoneCodeDictionary($db);

    $data = json_decode(file_get_contents("php://input"));

    if(isset($data->phone)) {
        if($fetchStateByCode->fetchStateByCode($data->phone)) {

            print_r(json_encode(array(
                'id' => $fetchStateByCode->id,
                'code' => $fetchStateByCode->code,
                'state' => $fetchStateByCode->state
            )));

        } else {
            echo json_encode(array('message' => "No records found!"));
        }

    } else {
        echo json_encode(array('message' => "Error: Phone is missing!"));
    }
} else {
    echo json_encode(array('message' => "Error: incorrect Method!"));
}
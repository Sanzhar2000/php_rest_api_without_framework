  

# PHP REST API without any framework

  

  

TThis is my test task for getting internship at VK.

  

## Contents

  

***/api*** folder contains all PHP files and endpoints for the required operations

***/config*** folder contains Postgres database configurations

***/models*** folder contains a multiple models for database logic

  

## How to run this project


1. Clone this repo.

2. Create a database with tables described in models folder.

3. Configure your own database configurations in ***config/Database.php*** file.

4. run Redis

5. php -S localhost:8000 -t ./

6. Check your endpoints.

  

## Endpoints USER

1. GET METHOD `http://localhost:8000/api/users/get_all.php`
![GET METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2021.52.14.png)
2. GET METHOD `http://localhost:8000/api/users/get_one.php`
![GET METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2021.53.50.png)



## Endpoints PHONE

1. POST METHOD `http://localhost:8000/api/phones/post_one.php`
![POST METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2021.55.32.png)
2. GET METHOD `http://localhost:8000/api/phones/search_by_substring_phone.php`
![GET METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2022.05.20.png)



## Endpoints REVIEW

1. POST METHOD `http://localhost:8000/api/reviews/post_one.php`
![POST METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2022.07.11.png)
2. PUT METHOD `http://localhost:8000/api/reviews/put_review.php`
![PUT METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2022.10.36.png)
3. GET METHOD `http://localhost:8000/api/reviews/get_all_reviews_by_phone_id.php`
![GET METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2022.11.30.png)



# Endpoints Get State by a phone code

1. GET METHOD `http://localhost:8000/api/reviews/post_one.php`
![GET METHOD](screenshots_for_readme/Screen%20Shot%202022-04-02%20at%2022.13.47.png)



# Task 7 Высокие нагрузки:

При высоких нагрузках на методы, самым простым способ будет кэширование на базовом уровне,
для этого как пример в **Endpoints USER 1** использовал **REDIS** для кэширования, дабы избежать 
множества обращений в бд. Так же можно было бы использовать только **один connection** в **БД**
используя паттерн проектирования **Singleton**, также можно было бы еще и использовать несколько
экземплятов этого приложения, то есть **распараллелить**. Использования очередей, и различных 
message brokers таких как **RabbitMQ**, **Apache Kafka** для целей данного приложения не оправданы.
